let array_code = [
    "1.",
    "1.1.",
    "1.2.",
    "1.3.",
    "1.4.",
    "1.1.1.",
    "1.1.2.",
    "1.1.3.",
    "1.2.1.",
    "1.2.2.",
    "1.3.1.",
    "1.3.2.",
    "1.3.3.",
    "1.3.4.",
    "1.4.1.",
    "1.4.3.",
  ];
  
  let result = {};
  
  for (let i = 0; i < array_code.length; i++) {
    let number = array_code[i].split("");
    if (number.length === 2) {
      result[number[0]] = {};
    }
    if (number.length === 4) {
      result[`${number[0]}`][number[number.length - 2]] = {};
    }
    if (number.length === 6) {
      result[`${number[0]}`][`${number[2]}`][number[number.length - 2]] =
        array_code[i];
    }
  }
  
  console.log(result);
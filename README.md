# Frontend Developer MileApp Test

## 1. Do you prefer vuejs or reactjs? Why ?

I prefer using React JS, because the React community has also been very innovative, while vue developed the state management by their own team, react somehow give those jobs to the community, it's making the community of Reactjs is more lively and the library supports from community was uncountable. After that, react somehow use ES5 and above oftenly, so for me it was handful and not take time too long to adapt with these framework.

Vue has a big community too, and in the future i want to try these framework because the update of Vue 3.0 name was One Piece and i love it.

## 2. What complex things have you done in frontend development ?

The one that i care the most from the start of my journey in learning javascript was the design system, that include architecture, folder structure, file structure and all of design pattern for building the app. Design system used to manage the code so it was more maintanable and more understandable even for juniors. After that come the CSS, that have all kind of magic that really confusing with keyframes, responsive, animation and many more of it, and i really concern in CSS study right now.

## 3. why does a UI Developer need to know and understand UX? how far do you understand it?

the purpose of UI to create the result of all the UX researcher team was found, and implemented it for the better journey of users when using the app. It really matters to make the users love and comfort to use our app regularly, or passively use our app, because if the users love our app, it was definitely because of the calculated design, easy to use, and make the complex feature in app like the easy one, and the impact of those ui/ux implementation was the amount of active users in our app.

## 4. Give your analysis results regarding https://taskdev.mile.app/login from the UI / UX side!

I have create the point list for the analysis :

- Choosed color was pallete color, with the main color is soft blue. Blue was the safe color to use and seems friendly.
- Simple and elegant, the purpose of the page can directly get by user, for this page the purpose was to login or to create the demo account.
- Some miss placed information, i think the place for call numbers not in navbar, but can merge with the copyright in the footers, for better contact information given to the users.

## 5. Mile-App-Login-Page Test Project

This project was deployed to the netlify!
you can visit this link below :
https://flamboyant-galileo-55f192.netlify.app/

### Available Scripts

In the project directory, you can run:

### `yarn start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.
You will also see any lint errors in the console.

## 6. logic problems saved in the 6-logic-test folder. same with this repository.

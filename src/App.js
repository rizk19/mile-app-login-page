import LangState from "./config/Context/LangState";
import LoginPage from "./pages/LoginPage"

function App() {
  return (
    <LangState>
      <LoginPage></LoginPage>
    </LangState>
  );
}

export default App;

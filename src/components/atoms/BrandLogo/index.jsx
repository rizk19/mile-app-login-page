import React from "react";
import styled from "styled-components";

const Image = styled.img`
    width:50%;
    height:auto;
`

const BrandLogo = (props) => {
    return (
        <Image src="https://taskdev.mile.app/69ece7b1819e760b63623e810922b59e.png"></Image>
    )
}

export default BrandLogo
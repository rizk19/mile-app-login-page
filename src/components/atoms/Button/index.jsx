import React from "react";
import styled from "styled-components";
import "./button.css";
const CustomButton = styled.button`
    display:flex;
    cursor: pointer;
    align-items: center;
    justify-content: center;
    border: 0;
    width: 100%;
    padding: 10px;
    background-color:#38BFFC;
    color : white;
    border-radius:4px;
`

const Button = (props) => {
    console.log(props.loading);
    return (
        <CustomButton {...props} {...props.rest}>
            {!props.loading ? props.children : <div class="loadingio-spinner-spin-pa36e6spqfr"><div class="ldio-zexca91ny4c">
                <div><div></div></div><div><div></div></div><div><div></div></div><div><div></div></div><div><div></div></div><div><div></div></div><div><div></div></div><div><div></div></div>
            </div></div>}
        </CustomButton>
    )
}

export default Button
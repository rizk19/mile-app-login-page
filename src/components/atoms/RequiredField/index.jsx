import React from "react";
import styled, { keyframes } from "styled-components";

const appearLabel = keyframes`
  0% {
    opacity: 0;
  };
  100% {
    opacity: 1;
  };
`;

const disappearLabel = keyframes`
  0% {
    opacity: 1;
  };
  100% {
    opacity: 0;
  };
`;

const Input = styled.input.attrs({ type: props => props.type || "text" })`
  width:92%;
  padding: 10px;
  border-radius:4px;
    border:1px solid grey;
  transition: all 0.3s ease-in-out;
  height:95%;
  &:focus {
    /* background: hsl(
      ${({ valLength }) => Math.min(valLength ? valLength * 18 : 0, valLength ? 100 : 0)},
      60%,
      60%
    ); */
    border:1.5px solid #38BFFC;
    outline: none;
  }
  
  @media (max-width: 330px) {
    width:89%;
}
`;

const InputNumber = styled.input.attrs({ type: "number" })`
  width:72%;
  padding: 10px;
  border-top-right-radius:4px;
  border-bottom-right-radius:4px;
    border:1px solid grey;
  transition: all 0.3s ease-in-out;
  height:95%;
  &:focus {
    /* background: hsl(
      ${({ valLength }) => Math.min(valLength ? valLength * 18 : 0, valLength ? 100 : 0)},
      60%,
      60%
    ); */
    border:1.5px solid #38BFFC;
    outline: none;
  }
`;

const Box = styled.div`
  width:100%;
  display:flex;
  flex-direction: column;
`

const BoxNumber = styled.div`
  width:100%;
  display:flex;
  flex-direction: row;
`

const CountryNumber = styled.div`
  width:20%;
  color: black;
  display: flex;
	flex-direction: column;
	flex-wrap: nowrap;
	justify-content: center;
	align-items: center;
  align-content: center;
  background-color: lightgrey;
  border:1px solid grey;
  border-right-width:0px;
  border-top-left-radius:4px;
  border-bottom-left-radius:4px;
`

const Label = styled.p`
  color:black;
  font-size:12px;
  width:auto;
margin-top: 3px;
margin-bottom: 3px;
font-weight: 600;
`

const HelperText = styled.p`
color: red;
font-size:10px;
width:100%;
animation: ${({ getValues }) => getValues ? appearLabel : disappearLabel} 0.3s ease-in;
margin-top: 3px;
margin-bottom: 8px;
text-align:end;
`


const RequiredField = (props) => {
  const { inputRef, label, helper, helpertext, lang, name, getValues, type } = props;
  return (
    <Box>
      <Label>{label}</Label>
      {type === "text" &&
        <Input valLength={getValues[name] && getValues[name].length} ref={inputRef} name={name} {...props} />
      }
      {type === "number" &&
        <BoxNumber>
          <CountryNumber>+62</CountryNumber>
          <InputNumber type={type} valLength={getValues[name] && getValues[name].length} ref={inputRef} name={name} {...props} />
        </BoxNumber>
      }
      <HelperText>{helper && helper[name]?.type === "required" &&
        `${label} ${helpertext.required[lang]}`}
        {helper && helper[name]?.type === "minLength" &&
          `${label} ${helpertext.length[lang]}`}
        {helper && helper[name]?.type === "min" &&
          `${helpertext.number[lang]}`}
        {helper && helper.email && helper[name]?.type === "pattern" &&
          `${helper.email.message}`}</HelperText>
    </Box>
  )
}

export default RequiredField;
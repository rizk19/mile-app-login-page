import React from "react";
import styled, { keyframes } from "styled-components";

const appearLabel = keyframes`
  0% {
    opacity: 0;
  };
  100% {
    opacity: 1;
  };
`;

const disappearLabel = keyframes`
  0% {
    opacity: 1;
  };
  100% {
    opacity: 0;
  };
`;

const Input = styled.input.attrs({ type: props => props.type || "text" })`
  width:93%;
  padding: 10px;
  border-radius:4px;
    border:1px solid #000;
  transition: all 0.3s ease-in-out;
  height:95%;
  &:focus {
    outline: none;
    border:1.5px solid #38BFFC;
  }
  @media (max-width: 330px) {
    width:89%;
}
`;

const Box = styled.div`
  width:100%;
  margin-top:3px;
  position: relative;
`

const Label = styled.p`
  color:black;
  font-size:9px;
  position:absolute;
  background-color:white;
  margin-top:-5px;
  left: 6px;
  width:auto;
  padding-left:3px;
  padding-right:3px;
  animation: ${({ value }) => value ? appearLabel : disappearLabel} 0.2s ease-in;
  &:focus {
    color:#38BFFC;
  }
`

const HelperText = styled.p`
color: red;
font-size:10px;
width:100%;
animation: ${({ value }) => value ? appearLabel : disappearLabel} 0.2s ease-in;
`

const TextField = (props) => {
  const { inputRef, label, helper, name, getValues } = props;
  return (
    <Box>
      {getValues[name] !== "" && label &&
        <Label value={getValues[name]}>{label}</Label>
      }
      <Input ref={inputRef} name={name} {...props} />
      <HelperText>{helper && helper[name]?.type === "required" &&
        `${label} harus terisi!`}
        {helper && helper[name]?.type === "minLength" &&
          `${label} belum mencapai batas minimum!`}</HelperText>
    </Box>
  )
}

export default TextField
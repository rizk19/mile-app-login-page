import React, { useState } from "react";
import styled from "styled-components";
import { TextField, Button, ToastCenter } from "../../atoms";
import { useForm } from "react-hook-form";

const duration = 1000;

const SectionOne = styled.form`
	display: flex;
	flex-direction: column;
	flex-wrap: nowrap;
	justify-content: flex-start;
	align-items: center;
	align-content: center;
    width: ${({ toggled }) => (toggled ? "100%" : "0")};
    opacity: ${({ toggled }) => (toggled ? "1" : "0")};
    margin-left: ${({ toggled }) => (toggled ? "0" : "-300px")};
    transition: all ${duration}ms ease-in-out;
    overflow:hidden;
`;

const Breaker = styled.div`
    margin-top: 16px;
`

const RegisterText = styled.p`
    text-align:center;
    color: black;
    display:flex;
    margin: 0;
    margin-top: 16px;
`

const RegisterLink = styled.span`
    color: #38BFFC;
    cursor: pointer;
`

const LoginForm = (props) => {
    const { status, toggled, inputText, lang, handleToggle } = props;
    const { register, errors, handleSubmit, getValues } = useForm();

    const [loading, setLoading] = useState(false)
    const onSubmit = data => {
        setLoading(true)
        setTimeout(() => {
            setLoading(false)
            ToastCenter.fire({
                icon: "success",
                title: `Login success`
            })
        }, 2500);
    };;
    return (

        <SectionOne status={status} toggled={toggled} onSubmit={handleSubmit(onSubmit)}>
            <TextField
                getValues={getValues()}
                name={inputText.input.name[lang]}
                inputRef={register({ required: true, minLength: 5 })}
                label={inputText.input.label[lang]}
                placeholder={inputText.input.placeholder[lang]}
                helper={errors}
            ></TextField>
            <Breaker />
            <Button loading={loading} type="submit">{inputText.button[lang]}</Button>
            <Breaker />
            <RegisterText>{inputText.registerText.question[lang]}</RegisterText>
            <RegisterText><RegisterLink onClick={handleToggle}>{inputText.registerText.link[lang]}</RegisterLink>&nbsp;{inputText.registerText.etc[lang]}</RegisterText>
        </SectionOne>
    )
}

export default LoginForm
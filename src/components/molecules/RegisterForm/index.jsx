import React, { useState } from "react";
import styled from "styled-components";
import { RequiredField, Button, ToastCenter } from "../../atoms";
import { useForm } from "react-hook-form";

const SectionTwo = styled.form`
  margin-right: ${({ toggled, status }) =>
    !toggled || status === "entering" ? "-300px" : "0"};
  width: 100%;
  margin-top:-20px;
`;

const Spacer = styled.div`
  margin-top:16px;
`

const RegisterForm = (props) => {
  const { status, toggled, inputText, lang } = props;
  const { register, errors, handleSubmit, getValues } = useForm();

  const [loading, setLoading] = useState(false)
  const onSubmit = data => { setLoading(true) };
  const onLoad = () => {
    setLoading(true)
    setTimeout(() => {
      setLoading(false)
      ToastCenter.fire({
        icon: "success",
        title: `Data success`
      })
    }, 2500);
  };

  return (
    <SectionTwo onSubmit={handleSubmit(onSubmit)} status={status} toggled={toggled}>
      <RequiredField
        type="text"
        getValues={getValues()}
        name={inputText.registerInput.name.name[lang]}
        inputRef={register({ required: true, minLength: 5 })}
        label={inputText.registerInput.name.label[lang]}
        placeholder={inputText.registerInput.name.placeholder[lang]}
        helpertext={inputText.registerInput.helpertext}
        helper={errors}
        lang={lang}
      ></RequiredField>
      <RequiredField
        type="text"
        getValues={getValues()}
        name={inputText.registerInput.email.name[lang]}
        lang={lang}
        inputRef={register({
          required: true,
          pattern: {
            value: /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i,
            message: inputText.registerInput.helpertext.pattern[lang]
          }
        })}
        label={inputText.registerInput.email.label[lang]}
        placeholder={inputText.registerInput.email.placeholder[lang]}
        helpertext={inputText.registerInput.helpertext}
        helper={errors}
      ></RequiredField>
      <RequiredField
        type="number"
        getValues={getValues()}
        name={inputText.registerInput.phonenumber.name[lang]}
        lang={lang}
        inputRef={register({
          min: {
            value: 999
          }
        })}
        label={inputText.registerInput.phonenumber.label[lang]}
        placeholder={inputText.registerInput.phonenumber.placeholder[lang]}
        helpertext={inputText.registerInput.helpertext}
        helper={errors}
      ></RequiredField>
      <RequiredField
        type="text"
        getValues={getValues()}
        lang={lang}
        inputRef={register({
          required: true,
          pattern: {
            value: /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i,
            message: inputText.registerInput.helpertext.pattern[lang]
          }
        })}
        name={inputText.input.name[lang]}
        label={inputText.input.label[lang]}
        placeholder={inputText.input.placeholder[lang]}
        helpertext={inputText.registerInput.helpertext}
        helper={errors}
      ></RequiredField>
      <Spacer></Spacer>
      <Button loading={loading} type="submit" onClick={onLoad}>Ajukan Demo</Button>
    </SectionTwo>
  )
}

export default RegisterForm
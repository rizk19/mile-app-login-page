import React from "react";
import "./index.scss";

const FooterSection = () => {

    return (
        <footer className="footer">
            <div className="img-footer"></div>
            <div className="footer-entity">
                <p className="footer-title">&copy;{' '}<a className="footer-link" href="https://github.com/rizk19"><strong>Copyright 2019 PT. Paket Informasi Digital. All Rights Reserved</strong></a> .2020</p>
                <p className="footer-title"><a className="footer-link" href="#12"><strong>+62 812-1133-5608</strong></a></p>
                <p className="footer-title"><a className="footer-link" href="https://github.com/rizk19">by rizk19.github.io</a></p>
            </div>
        </footer >
    )
}

export default FooterSection
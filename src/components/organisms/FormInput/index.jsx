import React, { useState, useContext } from "react";
import styled from "styled-components";
import { Transition } from "react-transition-group";
import { BrandLogo } from "../../atoms"
import { LoginForm, RegisterForm } from "../../molecules";
import LangContext from "../../../config/Context/LangContext.js"
import inputText from "./inputText";

const duration = 1000;

const FormLayout = styled.div`
  margin: 200px;
  padding: 70px 20px;
  width: 30%;
  background-color: white;
  -webkit-box-shadow: 0px 0px 35px -25px rgba(0,0,0,1);
-moz-box-shadow: 0px 0px 35px -25px rgba(0,0,0,1);
box-shadow: 0px 0px 35px -25px rgba(0,0,0,1);
border-radius: 4px;

@media only screen and (max-width : 768px) {
  width: 85%;
}
@media (max-width: 330px) {
  width: 95%;
  margin:0px;
  }
`;

const ContainerBrand = styled.div`
  display: flex;
	flex-direction: column;
	flex-wrap: wrap;
	justify-content: center;
	align-items: center;
	align-content: center;
  overflow: hidden;
  margin-bottom:30px;
`;

const Caption = styled.p`
    text-align:center;
    width:70%;
    margin-top: 30px;
`;

const Container = styled.div`
  color: white;
  display: flex;
  flex-direction:row;
  justify-content: center;
	align-items: center;
	align-content: center;
  overflow: hidden;
  width: 70%;
  margin: auto;
`;

const LoginPage = () => {
  const [toggled, setToggled] = useState(true)
  const langContext = useContext(LangContext);
  const { lang } = langContext;

  const handleToggle = () => {
    setToggled(!toggled)
  }

  return (
    <FormLayout>
      <ContainerBrand>
        <BrandLogo />
        <Caption>{inputText.caption[lang]}</Caption>
      </ContainerBrand>
      <Transition in={toggled} timeout={duration + 100}>
        {(status) => (
          <Container>
            <LoginForm handleToggle={handleToggle} status={status} toggled={toggled} inputText={inputText} lang={lang} />
            {(status === "entering" ||
              status === "exiting" ||
              !toggled) && (
                <RegisterForm status={status} toggled={toggled} inputText={inputText} lang={lang} />
              )}
          </Container>
        )}
      </Transition>
    </FormLayout>
  )
}

export default LoginPage
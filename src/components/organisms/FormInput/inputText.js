const inputText = {
    caption: {
      "id": "Masukkan informasi detail anda",
      "en": "Fill your detail Information"
    },
    input: {
      name: {
        "id": "nama-organisasi",
        "en": "organization-name"
      },
      label: {
        "id": "Nama Organisasi",
        "en": "Organization's Name"
      },
      placeholder: {
        "id": "Masukkan Nama Organisasi Anda",
        "en": "Enter your organization's name"
      }
    },
    registerInput: {
      name: {
        name: {
          "id": "nama-lengkap",
          "en": "full-name"
        },
        label: {
          "id": "Nama Lengkap",
          "en": "Full name"
        },
        placeholder: {
          "id": "Masukkan nama lengkap",
          "en": "Enter your full name"
        },
      },
      email:{
        name: {
          "id": "email",
          "en": "email"
        },
        label: {
          "id": "Email",
          "en": "Email"
        },
        placeholder: {
          "id": "Masukkan Email",
          "en": "Enter your email address"
        },
      },
      phonenumber:{
        name: {
          "id": "phone-number",
          "en": "no-telepon"
        },
        label: {
          "id": "Nomor telepon",
          "en": "Phone number"
        },
        placeholder: {
          "id": "Masukkan nomor telepon",
          "en": "Enter your phone number"
        }
      },
      company:{
        name: {
          "id": "nama-perusahaan",
          "en": "company-name"
        },
        label: {
          "id": "Nama Perusahaan",
          "en": "Company Name"
        },
        placeholder: {
          "id": "Masukkan nama perusahaan",
          "en": "Enter your company name"
        }
      },
      helpertext: {
        required:{
          "id": " harus terisi!",
          "en": " field is required"
        },
        length:{
          "id": " belum mencapai batas minimum!",
          "en": " field hasn't react a minimum length!"
        },
        pattern:{
          "id": " Format email tidak sesuai!",
          "en": " Invalid email address!"
        },
        number:{
          "id": "Input harus angka!",
          "en": "Field must be a number!"
        }
      }
    },
    button: {
      "id": "Masuk",
      "en": "Login"
    },
    registerText: {
      question: {
        "id": "Belum terdaftar? ",
        "en": "Not registered yet? "
      },
      link: {
        "id": " Hubungi kami",
        "en": " Contact us"
      },
      etc: {
        "id": "untuk info lebih lanjut.",
        "en": "for more info."
      }
    }
}

export default inputText
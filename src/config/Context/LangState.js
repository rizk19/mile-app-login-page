import React,
{
    useState,
    useReducer
} from 'react'

import langReducer from "./langReducer";
import LangContext from "./LangContext";

const LangSwitch = (props) => {
    const [initialState] = useState({
        lang: "id"
    });
    
    const [resState, dispatch] = useReducer(langReducer, initialState);
    
    const changeLang = (params) => {
        dispatch({ type: "CHANGE_LANGUAGE", payload: "en" })
    }
    
    return (
        <LangContext.Provider
        value={{
            lang: resState.lang,
            changeLang: changeLang
        }}
        >
            {props.children}
        </LangContext.Provider>
    )
}

export default LangSwitch;
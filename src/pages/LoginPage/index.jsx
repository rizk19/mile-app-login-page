import React from "react";
import styled from "styled-components";
import FormInput from "../../components/organisms/FormInput"
import Footer from "../../components/organisms/Footer"

const Div = styled.div`
    height: 130vh;
    width: 100%;
    background-color: #eef8fd;
	display: flex;
	flex-direction: column;
	flex-wrap: nowrap;
	justify-content: center;
	align-items: center;
    align-content: center;
    background: url("https://taskdev.mile.app/d09f7f2de7d88fb5f1575273b8a26426.png");
    background-repeat:no-repeat;
    background-position-x: right;
    @media (max-width: 799px) and (min-width: 449px) {
    background:none;
    }
  
  @media (max-width: 450px) {
    background:none;
  }
  
  @media (max-width: 330px) {
      height:550px;
  }
`;

const LoginPage = () => {

    return (
        <Div>
            <FormInput />
            <Footer></Footer>
        </Div>
    )
}

export default LoginPage